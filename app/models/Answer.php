<?php
namespace Models;


	class Answer extends \WeDevs\ORM\Eloquent\Model {

		/**
		 * Name for table without prefix
		 *
		 * @var string
		 */
		protected $table = 'qa_answers';


		/**
		 * Columns that can be edited - IE not primary key and timestamps if being uses
		 */
		protected $fillable = [
			'answer','question_id'
		];

		/**
		 * Disable created_at and update_at columns, unless you have those.
		 */
		public $timestamps = false;

		/** Everything below this is best done in an abstract class that custom tables extend */

		/**
		 * Set primary key as ID, because WordPress
		 *
		 * @var string
		 */
		protected $primaryKey = 'id';

		/**
		 * Make ID guarded -- without this ID doesn't save.
		 *
		 * @var string
		 */
		protected $guarded = [ 'id' ];

		/**
		 * Overide parent method to make sure prefixing is correct.
		 *
		 * @return string
		 */
		public function getTable()
		{
			//In this example, it's set, but this is better in an abstract class
			if( isset( $this->table ) ){
				$prefix =  $this->getConnection()->db->prefix;
				return $prefix . $this->table;

			}

			return parent::getTable();
		}

    public function question()
    {
      return $this->belongsTo('Models\Question');
    }

		public function stats()
		{
			return $this->hasMany('Models\Stats','answer_id');
		}


	}
