<?php if (isset($_GET['type']) && isset($_GET['msg'])): ?>
  <div id="message" class="notice notice-<?php echo $_GET['type'] ?>">
    <p><?php echo $_GET['msg'] ?></p>
  </div>
<?php endif; ?>
