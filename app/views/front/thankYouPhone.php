<?php /* Template Name: ThankYou Page */ ?>
<?php
namespace Controllers;
use Models\Question;
use Models\Answer;
use Models\Stats;
$questions = Question::all();
$answers = Answer::all();
$nonce = wp_create_nonce('thankYouPhone');

global $wp;
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url( 'qa.css', __FILE__ ); ?>">
</head>
<body>
<div class="container">
	<?php
	echo '<img class="logo" src="' . plugins_url( 'images/ekocykl.png', __FILE__ ) . '" > ';
	?>
	<?php
	if (isset($_GET['answer']) && isset($_GET['email'])) {
		$unhashed_answer = base64_decode(htmlspecialchars($_GET["answer"]) );
		$unhashed_email = hex2bin(htmlspecialchars($_GET["email"]) );
		$answer = Answer::findOrFail((int)$unhashed_answer);
		$check = Stats::where('answer_id', $unhashed_answer)->where('mail', $unhashed_email)->get();
		$check_phone = Stats::where('answer_id', $unhashed_answer)->where('mail', $unhashed_email)->where('tel',NULL)->get();
	}
	?>


<?php if ($check_phone->isEmpty()): ?>
	<h3 class="thankYou">Dziękujemy za odpowiedź.</h3>
<?php else: ?>
	<?php $link = http_build_query(['answer'=>$_GET["answer"],'email'=>$_GET["email"]]); ?>
	<form class="form-tel" action="<?php echo esc_url(  get_permalink().'?'.$link ); ?>" method="post">
		<h3 class="podajnr">Podaj nr telefonu</h3>
		<input type="hidden" name="action" value="frontActionAddPhoneNumber">
		<input type="hidden" name="nonce" value="<?php echo $nonce ?>">
			<input class="nr_telefonu" maxlength="9" type="text" placeholder="Nr telefonu" name="tel" value="">
			<input class="button button-primary" type="submit" name="" value="Wyślij">
	</form>
<?php endif; ?>
</div>
</body>
</html>
