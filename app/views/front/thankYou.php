<?php /* Template Name: ThankYou Page */ ?>
<?php
namespace Controllers;
use Models\Question;
use Models\Answer;
use Models\Stats;
$questions = Question::all();
$answers = Answer::all();
$nonce = wp_create_nonce('thankyou');

global $wp;
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo plugins_url( 'qa.css', __FILE__ ); ?>">
</head>
<body>
<div class="container">
	<?php
	echo '<img class="logo" src="' . plugins_url( 'images/ekocykl.png', __FILE__ ) . '" > ';
	?>

	<h3 class="thankYou">Dziękujemy za odpowiedź.</h3>

</div>
</body>
</html>
