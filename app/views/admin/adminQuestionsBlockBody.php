<div class="wrap">
  <h3>Dodaj pytanie</h3>
  <form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
    <input type="hidden" name="action" value="adminActionAddQuestion">
    <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <div class="">
      <label for="question">Pytanie</label>
      <input type="text" name="question" value="">
    </div>
    <div class="">
      <input class="button button-primary" type="submit" name="" value="Dodaj pytanie">
    </div>
  </form>


<form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
  <input type="hidden" name="action" value="adminActionDeleteQuestion">
  <input type="hidden" name="nonce_delete" value="<?php echo $nonce_delete ?>">
  <div class="">
    <div class="">
      <label for="questionForDelete"><h3>Usuń pytanie</h3></label>
      <select class="" name="questionForDelete">
        <?php foreach ($questions as $q): ?>
          <option value="<?php echo $q->id ?> "><?php echo $q->question ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="">
    <input class="button button-primary" type="submit" name="" value="Usuń pytanie">
  </div>
</form>

<h3>Lista dodanych pytań</h3>
<ol>
<?php foreach ($questions as $q): ?>
  <li>
    <h3><?php echo $q->question; ?></h3>
  </li>
  <?php foreach ($answers as $a): ?>
    <?php if ($a->question_id == $q->id): ?>
        <h4> Odp: <?php echo $a->answer; ?></h4>
    <?php endif; ?>
  <?php endforeach; ?>
<?php endforeach; ?>
</ol>
</div>
