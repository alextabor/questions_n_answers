<?php
namespace Controllers;
use Models\Question;
use Models\Answer;
use Models\Stats;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
?>

<div class="wrap">
  <h2 id="plugintitle"><?php echo $title ?></h2>
</div>



<form class="" action="<?php echo esc_url( admin_url('admin-post.php') );?>" method="post">
  <input type="hidden" name="action" value="adminActionGenerateExcel">
  <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
  <label for="Answers"><h3>Wybierz odpowiedź</h3></label>
  <select class="" name="selectedAnswer">
  <?php foreach ($answers as $a): ?>
    <option value="<?php echo $a->id ?> "><?php echo 'Pytanie: '.$a->question->question.' Odpowiedź: '.$a->answer ?></option>
  <?php endforeach; ?>
  </select>
  <input class="button button-primary" style="background-color: #1A7343;" type="submit" name="" value="Generuj XLSX">
</form>



<?php if ( isset($_GET['raport']) ): ?>
<a class="button button-primary" style="background-color: #1A7343;" href="../Raport_<?php echo $_GET['raport'] ?>.xlsx">Pobierz XLSX</a>
<?php endif; ?>
<?php

$spreadsheet = new Spreadsheet();

$spreadsheet->getProperties()->setCreator("QA Plugin - swiatwebmasterow.pl");

$spreadsheet->getProperties()->setTitle("Raport wszystkich odpowiedzi");


foreach(range('A','Z') as $columnID) {
    $spreadsheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
}



// $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$sheet = $spreadsheet->getActiveSheet();


$sheet->setCellValue('A1', 'E-mail');
$sheet->setCellValue('B1', 'Pytanie');
$sheet->setCellValue('C1', 'Odpowiedź');
$sheet->setCellValue('D1', 'Data');
$sheet->setCellValue('E1', 'Telefon');
$sheet->setCellValue('G1', 'Pytanie / odp');
$sheet->setCellValue('H1', 'Kliknięcia');


$rows = 2;

foreach ($stats as $s) {
  $sheet->setCellValue('A'.$rows, $s->mail);
  $sheet->setCellValue('B'.$rows, $s->answer->question->question);
  $sheet->setCellValue('C'.$rows, $s->answer->answer);
  $sheet->setCellValue('D'.$rows, $s->data);
  $sheet->setCellValue('E'.$rows, $s->tel);
  $rows++;
}


$rows = 2;


foreach ($questions as $q) {
  $number_of_all_clicks = 0;
  $sheet->setCellValue('G'.$rows, $q->question);
  $spreadsheet->getActiveSheet()->getStyle('G'.$rows)
    ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
  $spreadsheet->getActiveSheet()->getStyle('G'.$rows)
    ->getFill()->getStartColor()->setARGB('FFFF0000');
  $answers_this_question = Answer::where('question_id', $q->id)->get();
  foreach ($answers_this_question as $atq) {
    $number_of_all_clicks += Stats::where('answer_id', $atq->id)->count();
  }
  $sheet->setCellValue('H'.$rows, $number_of_all_clicks);
  $rows++;
  $answers_this_question = Answer::where('question_id', $q->id)->get();
  foreach ($answers_this_question as $atq) {
    $sheet->setCellValue('G'.$rows, $atq->answer);
    $number_of_clicks = Stats::where('answer_id', $atq->id)->count();
    $sheet->setCellValue('H'.$rows, $number_of_clicks);
    $rows++;
  }
}



$writer = new Xlsx($spreadsheet);
$writer->save('../Raport.xlsx');





?>

<h3>Raport zawierający wszystkie pytania i odpowiedzi:</h3>
<a class="button button-primary" style="background-color: #1A7343;" href="../Raport.xlsx">Pobierz XLSX</a>
<h4>Podgląd:</h4>
<?php

$worksheet = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
$highestRow = $worksheet->getHighestRow(); // e.g. 10
$highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
$highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

echo '<table>' . "\n";
for ($row = 1; $row <= $highestRow; ++$row) {
    echo '<tr>' . PHP_EOL;
    for ($col = 1; $col <= $highestColumnIndex; ++$col) {
        $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
        echo '<td>' . $value . '</td>' . PHP_EOL;
    }
    echo '</tr>' . PHP_EOL;
}
echo '</table>' . PHP_EOL;
 ?>
