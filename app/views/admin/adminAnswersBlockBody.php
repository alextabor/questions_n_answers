<div class="wrap">
  <h3>Dodaj odpowiedź do pytania</h3>
  <form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
    <input type="hidden" name="nonce" value="<?php echo $nonce ?>">
    <input type="hidden" name="action" value="adminActionAddAnswer">

    <div class="">
      <label for="question">Pytanie</label>
      <select class="" name="question">
        <?php foreach ($questions as $q): ?>
          <option value="<?php echo $q->id ?> "><?php echo $q->question ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="">
      <label for="answer">Odpowiedź do pytania</label>
      <input type="text" name="answer" value="">
    </div>
    <div class="">
      <input class="button button-primary" type="submit" name="" value="Dodaj odpowiedź">

    </div>
  </form>



  <form class="" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
    <input type="hidden" name="action" value="adminActionDeleteAnswer">
    <input type="hidden" name="nonce_delete" value="<?php echo $nonce_delete ?>">
    <div class="">
      <div class="">
        <label for="answerForDelete"><h3>Usuń odpowiedź</h3></label>
        <select class="" name="answerForDelete">
          <?php foreach ($answers as $a): ?>
            <option value="<?php echo $a->id ?> "><?php echo 'Pytanie: '.$a->question->question.' Odpowiedź: '.$a->answer ?></option>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="">
      <input class="button button-primary" type="submit" name="" value="Usuń odpowiedź">
    </div>
  </form>



  <form class="" action="<?php echo esc_url( admin_url('admin-post.php') );?>" method="post">
    <input type="hidden" name="action" value="adminActionGenerateLinks">
    <input type="hidden" name="nonce_links" value="<?php echo $nonce_links ?>">
    <label for="Answers"><h3>Wybierz odpowiedź</h3></label>
    <select class="" name="selectedAnswer">
    <?php foreach ($answers as $a): ?>
      <option value="<?php echo $a->id ?> "><?php echo 'Pytanie: '.$a->question->question.' Odpowiedź: '.$a->answer ?></option>
    <?php endforeach; ?>
    </select>
    <input class="button button-primary" style="background-color: #1A7343;" type="submit" name="" value="Generuj linki">
  </form>

  <?php if ( isset($_GET['link']) ): ?>
    <?php $answer_link_id = $_GET['link']; ?>
    <?php foreach ($answers as $a): ?>
      <?php if ($a->id == $answer_link_id): ?>
        <h4>Podziękowanie:</h4>
        <textarea style="width: 100%;" name="<?php echo $a->id;  ?>" class="thankYou" id="<?php echo $a->id ; ?>" type="textarea" rows="1"><a href="<?php echo $a->generated_link ?>"><?php echo $a->answer ?></a> </textarea>
        <button onclick="copy('thankYou')" class="button button-primary copy-btn" >Skopiuj link do schowka</button>
        <h4>Podziękowanie z telefonem do wpisania:</h4>
        <textarea style="width: 100%;" name="<?php echo $a->id; ?>" class="thankYouPhone" id="<?php echo $a->id; ?>" type="textarea" rows="1"><a href="<?php echo $a->generated_phone_link ?>"><?php echo $a->answer ?></a> </textarea>
        <button onclick="copy('thankYouPhone')" class="button button-primary copy-btn" >Skopiuj link do schowka</button>
      <?php endif; ?>
    <?php endforeach; ?>
  <?php endif; ?>

</div>

<script>
function copy(copyText) {
  if (copyText == 'thankYou') {
    var copyTy = document.getElementsByClassName("thankYou");
    console.log(copyTy);
      copyTy[0].select();
      document.execCommand("Copy");
  }
  else if (copyText == 'thankYouPhone') {
    var copyTyP = document.getElementsByClassName("thankYouPhone");
    console.log(copyTyP);
      copyTyP[0].select();
      document.execCommand("Copy");
  }
}
</script>
