<?php
namespace Controllers;
use Models\Question;
use Models\Answer;
use Models\Stats;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AdminActionsController
{

  protected $functions = array();

  public function __construct()
  {
    $this->functions = get_class_methods($this);
  }
  public function init()
  {
    foreach ($this->functions as $function) {
      add_action('admin_post_'.$function,array($this,$function));
    }
  }

  public function adminActionAddQuestion()
  {
    if (wp_verify_nonce($_POST['nonce'],'addQuestion')) {
      $question = new Question;
      $question->question = sanitize_text_field($_POST['question']);
      if ($question->question) {
        $question->save();
        $query = http_build_query(['type'=>'success','msg'=>'Dodano pomyślnie']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      } else {
        $query = http_build_query(['type'=>'error','msg'=>'Pole nie może zostać puste']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      }

    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
    }
  }

  public function adminActionAddAnswer()
  {
    if (wp_verify_nonce($_POST['nonce'],'addAnswer')) {
      $question = Question::findOrFail((int)$_POST['question']);
      $answer = new Answer;
      $answer->answer = sanitize_text_field($_POST['answer']);
      $question->answers()->save($answer);
      $hashed_id = base64_encode($answer->id);
      $link = http_build_query(['answer'=>$hashed_id,'email'=>'746573746F77794074776F6A61646F6D656E612E706C']);
      $answer->generated_link = get_home_url().'/thankyou/?&'.$link;
      $answer->generated_phone_link = get_home_url().'/thankyouphone/?&'.$link;
      $question->answers()->save($answer);
      $query = http_build_query(['type'=>'success','msg'=>'Dodano pomyślnie']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    }
  }


  public function adminActionDeleteQuestion()
  {
    if (wp_verify_nonce($_POST['nonce_delete'],'deleteQuestion')) {
      $question = Question::findOrFail((int)$_POST['questionForDelete']);
      if ($question->question) {

      $answers = Answer::where('question_id', $question->id)->get();

      foreach ($answers as $answer) {
        $stats = Stats::where('answer_id', $answer->id)->get();
        foreach ($stats as $stat) {
              $stat->delete();
          }
          $answer->delete();
      }
      $question->delete();

        $query = http_build_query(['type'=>'success','msg'=>'Usunięto pomyślnie']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      } else {
        $query = http_build_query(['type'=>'error','msg'=>'Nie zaznaczono pytania']);
        exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
      }

    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_questions&'.$query)));
    }
  }


  public function adminActionDeleteAnswer()
  {
    if (wp_verify_nonce($_POST['nonce_delete'],'deleteAnswer')) {
      $answer = Answer::findOrFail((int)$_POST['answerForDelete']);
      if ($answer->answer) {

      $stats = Stats::where('answer_id', $answer->id)->get();

      foreach ($stats as $stat) {
            $stat->delete();
        }

      $answer->delete();

        $query = http_build_query(['type'=>'success','msg'=>'Usunięto pomyślnie']);
        exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
      } else {
        $query = http_build_query(['type'=>'error','msg'=>'Nie zaznaczono odpowiedzi']);
        exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
      }

    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    }
  }


  public function adminActionGenerateLinks()
  {
    if (wp_verify_nonce($_POST['nonce_links'],'generateLinks')) {
      $answer = Answer::findOrFail((int)$_POST['selectedAnswer']);
      $query = http_build_query(['type'=>'success','msg'=>'Wygenerowano', 'link'=>$answer->id]);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_answers&'.$query)));
    }
  }



  public function adminActionGenerateExcel()
  {
    if (wp_verify_nonce($_POST['nonce'],'GenerateExcel')) {
      $answer = Answer::findOrFail((int)$_POST['selectedAnswer']);
      $spreadsheet_selected = new Spreadsheet();

      $spreadsheet_selected->getProperties()->setCreator("QA Plugin - swiatwebmasterow.pl");

      $spreadsheet_selected->getProperties()->setTitle("Raport jednej odpowiedzi");


      foreach(range('A','Z') as $columnID) {
          $spreadsheet_selected->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
      }
      $sheet_selected = $spreadsheet_selected->getActiveSheet();

      $sheet_selected->setCellValue('A1', 'E-mail');
      $sheet_selected->setCellValue('B1', 'Pytanie');
      $sheet_selected->setCellValue('C1', 'Odpowiedź');
      $sheet_selected->setCellValue('D1', 'Data');
      $sheet_selected->setCellValue('E1', 'Telefon');
      $sheet_selected->setCellValue('G1', 'Pytanie / odp');
      $sheet_selected->setCellValue('H1', 'Kliknięcia');

      $rows = 2;

      $stats = Stats::where('answer_id', $answer->id)->get();

      foreach ($stats as $s) {
        $sheet_selected->setCellValue('A'.$rows, $s->mail);
        $sheet_selected->setCellValue('B'.$rows, $s->answer->question->question);
        $sheet_selected->setCellValue('C'.$rows, $s->answer->answer);
        $sheet_selected->setCellValue('D'.$rows, $s->data);
        $sheet_selected->setCellValue('E'.$rows, $s->tel);
        $rows++;
      }

      $rows = 2;


      $sheet_selected->setCellValue('G'.$rows, $answer->question->question);

      $spreadsheet_selected->getActiveSheet()->getStyle('G'.$rows)
        ->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
      $spreadsheet_selected->getActiveSheet()->getStyle('G'.$rows)
        ->getFill()->getStartColor()->setARGB('FFFF0000');

      $this_answers = Answer::where('question_id', $answer->question->id)->get();

      $number_of_all_clicks = 0;

      foreach ($this_answers as $ta) {
        $number_of_all_clicks += Stats::where('answer_id', $ta->id)->count();
      }

      $sheet_selected->setCellValue('H'.$rows, $number_of_all_clicks);

      $rows++;

      foreach ($this_answers as $ta) {
        $sheet_selected->setCellValue('G'.$rows, $ta->answer);
        $number_of_clicks = Stats::where('answer_id', $ta->id)->count();
        $sheet_selected->setCellValue('H'.$rows, $number_of_clicks);
        $rows++;
      }


      $writer = new Xlsx($spreadsheet_selected);
      $writer->save("../Raport_$answer->answer.xlsx");

      $query = http_build_query(['type'=>'success','msg'=>'Wygenerowano', 'raport'=>$answer->answer]);
      exit(wp_redirect(admin_url('admin.php?page=qa_stats&'.$query)));
    } else {
      $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      exit(wp_redirect(admin_url('admin.php?page=qa_stats&'.$query)));
    }
  }




}
