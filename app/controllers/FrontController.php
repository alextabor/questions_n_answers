<?php
namespace Controllers;
use Core\View;
use Models\Question;
use Models\Answer;
use Models\Stats;

class FrontController
{

  public function init()
  {
    add_filter( 'after_setup_theme', array($this,'thankYouPage') );
    add_filter( 'after_setup_theme', array($this,'thankYouPhonePage') );
    add_filter( 'page_template', array($this,'get_thankyou') );

          // add_action('init',array($this,'frontThankYouBlockBody'));
          // add_action('init',array($this,'frontThankYouPhoneBlockBody'));
  }


  /**
   * Metoda wyswietlajaca widok Pluginu
   * @return [type] [description]
   */

   public function thankYouPage() {

     // Initialize the page ID to -1. This indicates no action has been taken.
     $post_id = -1;

     $author_id = 1;
     $slug = 'thankyou';
     $title = 'Thank You';

     // If the page doesn't already exist, then create it
     if( null == get_page_by_title( $title ) ) {

       // Set the post ID so that we know the post was created successfully
       $post_id = wp_insert_post(
         array(
           'comment_status'	=>	'closed',
           'ping_status'		=>	'closed',
           'post_author'		=>	$author_id,
           'post_name'		=>	$slug,
           'post_title'		=>	$title,
           'post_status'		=>	'publish',
           'post_type'		=>	'page'
         )
       );

     // Otherwise, we'll stop
     } else {

           // Arbitrarily use -2 to indicate that the page with the title already exists
           $post_id = -2;

     }

   }


   public function thankYouPhonePage() {

     // Initialize the page ID to -1. This indicates no action has been taken.
     $post_id = -1;

     $author_id = 1;
     $slug = 'thankyouphone';
     $title = 'Thank You Phone';

     // If the page doesn't already exist, then create it
     if( null == get_page_by_title( $title ) ) {

       // Set the post ID so that we know the post was created successfully
       $post_id = wp_insert_post(
         array(
           'comment_status'	=>	'closed',
           'ping_status'		=>	'closed',
           'post_author'		=>	$author_id,
           'post_name'		=>	$slug,
           'post_title'		=>	$title,
           'post_status'		=>	'publish',
           'post_type'		=>	'page'
         )
       );

     // Otherwise, we'll stop
     } else {

           // Arbitrarily use -2 to indicate that the page with the title already exists
           $post_id = -2;

     }

   }





   public function get_thankyou( $page_template )
   {
       if ( is_page('thankyou') ) {
           $page_template = QA_DIR . '/app/views/front/thankYou.php';
       }

       if ( is_page('thankyouphone') ) {
           $page_template =  QA_DIR . '/app/views/front/thankYouPhone.php';
       }

       return $page_template;
   }

  public function frontThankYouBlockBody()
  {
    $questions = Question::all();
    $answers = Answer::all();
    $nonce = wp_create_nonce('thankyou');
    $view = new View('front.thankYou');
    $view->setVariables('questions',$questions);
    $view->setVariables('answers',$answers);
    $view->setVariables('nonce',$nonce);
    echo $view->render();
  }

  public function frontThankYouPhoneBlockBody()
  {
    $questions = Question::all();
    $answers = Answer::all();
    $nonce = wp_create_nonce('thankYouPhone');
    $view = new View('front.thankYouPhone');
    $view->setVariables('questions',$questions);
    $view->setVariables('answers',$answers);
    $view->setVariables('nonce',$nonce);
    echo $view->render();
  }


}
