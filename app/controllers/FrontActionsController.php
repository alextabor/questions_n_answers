<?php
namespace Controllers;
use Models\Question;
use Models\Answer;
use Models\Stats;
global $wp;

class FrontActionsController
{

  protected $functions = array();

  public function __construct()
  {
    $this->functions = get_class_methods($this);
  }

  public function init()
  {
    foreach ($this->functions as $function) {
      add_action('init',array($this,$function));
    }
  }


  public function frontActionAddPhoneNumber()
  {
      if (wp_verify_nonce($_POST['nonce'],'thankYouPhone')) {
        $unhashed_answer = base64_decode(htmlspecialchars($_GET["answer"]) );
        $unhashed_email = hex2bin(htmlspecialchars($_GET["email"]) );
        $phone_number = sanitize_text_field($_POST['tel']);
        $check_phone = Stats::where('answer_id', $unhashed_answer)->where('mail', $unhashed_email)->where('tel',NULL)->get();
        if (!$check_phone->isEmpty()) {
        Stats::where('answer_id', $unhashed_answer)
            ->where('mail', $unhashed_email)
            ->update(['tel' => $phone_number]);
        }
        $query = http_build_query(['type'=>'success','msg'=>'Dodano numer telefonu']);
      } else {
        $query = http_build_query(['type'=>'error','msg'=>'Nieoczekiwany błąd']);
      }
  }

  public function frontActionAddStats()
  {
    if (isset($_GET['answer']) && isset($_GET['email'])) {
    	$unhashed_answer = base64_decode(htmlspecialchars($_GET["answer"]) );
    	$unhashed_email = hex2bin(htmlspecialchars($_GET["email"]) );
      $answer = Answer::findOrFail((int)$unhashed_answer);
      $check = Stats::where('answer_id', $unhashed_answer)->where('mail', $unhashed_email)->get();
      $check_phone = Stats::where('answer_id', $unhashed_answer)->where('mail', $unhashed_email)->where('tel',NULL)->get();
      if (!filter_var($unhashed_email, FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Invalid email format";
      }
      else {
        if ($check->isEmpty()) {
          $stat = new Stats;
          $stat->mail = $unhashed_email;
          $answer->stats()->save($stat);
        }
      }

    }
  }




}
