<?php

namespace Core;

use Controllers\AdminController;
use Controllers\AdminActionsController;
use Controllers\FrontController;
use Controllers\FrontActionsController;
class Init
{
  public function init()
  {
    $admin = new AdminController;
    $admin->init();

    $adminActions = new AdminActionsController;
    $adminActions->init();

    $front = new FrontController;
    $front->init();

    $frontActions = new FrontActionsController;
    $frontActions->init();
  }
}
