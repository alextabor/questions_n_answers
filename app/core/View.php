<?php
namespace Core;
/**
 * Klasa tworząca widoki
 */


class View
{
  protected $_variables = array() ;
  protected $_path;

    public function __construct($_path = null)
    {
      $this->_path = $_path;
    }

    /**
     * Get the value of Klasa tworząca widoki
     *
     * @return mixed
     */
    public function getVariables()
    {
        return $this->_variables;
    }

    /**
     * Set the value of Klasa tworząca widoki
     *
     * @param mixed _variables
     *
     * @return self
     */
    public function setVariables($name, $variable)
    {
        $this->_variables[$name] = $variable;

        return $this;
    }

    /**
     * Get the value of Path
     *
     * @return mixed
     */
    public function getPath()
    {
        $this->_path = str_replace('.', DIRECTORY_SEPARATOR, $this->_path);
        return $this->_path;
    }

    /**
     * Set the value of Path
     *
     * @param mixed _path
     *
     * @return self
     */
    public function setPath($_path)
    {
        $this->_path = $_path;

        return $this;
    }

    /**
     * Wyswietlenie widoku
     * @return [type] [description]
     */
    public function render()
    {
      if ($this->_path == null) {
        return '';
      }

      ob_start();
      extract($this->_variables);
      require QA_DIR  . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR . 'message' . DIRECTORY_SEPARATOR . 'message.php';
      require QA_DIR . DIRECTORY_SEPARATOR . 'app'. DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $this->getPath() .'.php';
      // $this->requireFlashMessage();
      $result = ob_get_contents();
      ob_get_clean();

      return $result;

    }

    // public function requireFlashMessage()
    // {
    //   require plugin_dir_path( __FILE__ ). '../../' . DIRECTORY_SEPARATOR . 'views'. DIRECTORY_SEPARATOR . 'flash' . DIRECTORY_SEPARATOR . 'message.php';
    // }

}
