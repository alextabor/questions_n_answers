<?php

namespace Core;

class Db
{
  protected $wpdb;
  protected $charset_collate;
  protected $questionsTable;
  protected $clientsTable;

  public function __construct()
  {
    global $wpdb;
    $this->wpdb = $wpdb;
    $this->charset_collate = $this->wpdb->get_charset_collate();
    $this->questionsTable = $this->wpdb->prefix . 'qa_questions';
    $this->answersTable = $this->wpdb->prefix . 'qa_answers';
    $this->clientsTable = $this->wpdb->prefix . 'qa_clients';
    $this->statsTable = $this->wpdb->prefix . 'qa_stats';
    register_activation_hook(__FILE__,array($this,'createDatabases'));
  }
  public function createDatabases()
  {

if($this->wpdb->get_var("SHOW TABLES LIKE '$this->questionsTable'") != $this->questionsTable) {
    $sql = "
        CREATE TABLE $this->questionsTable(
          id INT NOT NULL AUTO_INCREMENT,
          question VARCHAR(255) NOT NULL,
          PRIMARY KEY (id)
        );

        CREATE TABLE $this->answersTable(
          id INT NOT NULL AUTO_INCREMENT,
          question_id INT( 11 ) NOT NULL ,
          answer VARCHAR(255) NOT NULL,
          generated_link VARCHAR(255),
          generated_phone_link VARCHAR(255),
          PRIMARY KEY (id),
          FOREIGN KEY (question_id) REFERENCES $this->questionsTable(id)
        ) ;

        CREATE TABLE $this->statsTable(
          id INT NOT NULL AUTO_INCREMENT,
          answer_id INT( 11 ) NOT NULL ,
          mail VARCHAR(255),
          tel VARCHAR( 9 ),
          data TIMESTAMP,
          PRIMARY KEY (id),
          FOREIGN KEY (answer_id) REFERENCES $this->answersTable(id)
        ) ;


    ";



      require_once(ABSPATH. 'wp-admin/includes/upgrade.php');

      // foreach ($sql as $s) {
        dbDelta($sql);
      // }
      //
    }
  }
}
