<?php /* Template Name: ThankYou Page */ ?>
<?php

global $wp;
// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<p>
	Dziekujemy test
</p>

<?php $id_odpowiedzi = hex2bin(htmlspecialchars($_GET["answer"]) ); ?>
<p>
	Odp o ID: <?php echo $id_odpowiedzi; ?>
	Email: <?php echo htmlspecialchars($_GET["email"]) ?>
</p>
<p>
	Czas znalezc odpowiedz i pytanie do ktorej nalezy:
	<?php foreach ($questions as $q): ?>
	  <p><?php echo $q->question ?></p>
	  <p><?php echo $q->id ?></p>
	<?php endforeach; ?>
	<?php foreach ($answers as $a): ?>
	  <p><?php echo $a->answer ?> - <?php echo $a->question->question ?></p>
	<?php endforeach; ?>
</p>



<form class="" action="<?php echo esc_url( home_url( $wp->request ) ); ?>" method="post">
	<input type="hidden" name="action" value="frontActionAddPhoneNumber">
	<input type="hidden" name="nonce" value="<?php echo $nonce ?>">
	<div class="">
		<label for="tel">Nr telefonu:</label>
		<input type="text" name="tel" value="">
	</div>
	<div class="">
		<input class="button button-primary" type="submit" name="" value="Podaj numer telefonu">
	</div>
</form>


/* Omit closing PHP tag to avoid "Headers already sent" issues. */
