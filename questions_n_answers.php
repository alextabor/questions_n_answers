<?php
/**
* Plugin Name: Questions&Answers
* Description: Plugin do tworzenia pytań wraz z generowaniem URL
* Version: 1.0
* Author: Aleksander Tabor, Mateusz Prasał
* License: GPL
*/

require 'vendor/autoload.php';

if(! defined('ABSPATH')) exit;


if( ! defined('QA_DIR')){
        define('QA_DIR',__DIR__);
}



use Core\Db;
use Core\Init;
$db = new Db;
$db->createDatabases();

$init = new Init;
$init->init();
